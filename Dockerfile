FROM denvazh/java:openjdk8-jre
ADD gitlabService-1.jar /opt/
EXPOSE 4010
CMD ["java","-jar","/opt/gitlabService-1.jar"]