package com.gitlab;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GitlabController {
	private Runtime runtime = Runtime.getRuntime();
	@RequestMapping("/")
	public InfraInformation getInfraDetails() {

		String ipAddress = "unknown";
		try {
			ipAddress = InetAddress.getLocalHost().getHostAddress();
			} catch (IOException e) {
			e.printStackTrace();
		}

		InfraInformation infraInformation = new InfraInformation();
		infraInformation.setServiceName("Gitlab Service");
		infraInformation.setCpuCount(runtime.availableProcessors());
		infraInformation.setHostname(System.getenv("HOSTNAME"));
		infraInformation.setIpAddress(ipAddress);
		infraInformation.setOsArchitecture(System.getProperty("os.arch"));
		infraInformation.setOsName(System.getProperty("os.name"));
		infraInformation.setOsVersion(System.getProperty("os.version"));
		infraInformation.setTotalRuntimeMemory(Runtime.getRuntime().totalMemory());
		infraInformation.setFreeRuntimeMemory(Runtime.getRuntime().freeMemory());
		return infraInformation;
	}
	@RequestMapping("/health")
	public int healthCheck(HttpServletResponse httpServletResponse) {
		return httpServletResponse.getStatus();
	}
}
