package com.gitlab;

public class InfraInformation {
	private String serviceName;
	private String osName;
	private String osVersion;
	private String osArchitecture;
	private int cpuCount;
	private String hostname;
	private String ipAddress;
	private long totalRuntimeMemory;
	private long freeRuntimeMemory;
	public String getOsName() {
		return osName;
	}
	public void setOsName(String osName) {
		this.osName = osName;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getOsArchitecture() {
		return osArchitecture;
	}
	public void setOsArchitecture(String osArchitecture) {
		this.osArchitecture = osArchitecture;
	}
	public int getCpuCount() {
		return cpuCount;
	}
	public void setCpuCount(int cpuCount) {
		this.cpuCount = cpuCount;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public long getTotalRuntimeMemory() {
		return totalRuntimeMemory;
	}
	public void setTotalRuntimeMemory(long totalRuntimeMemory) {
		this.totalRuntimeMemory = totalRuntimeMemory;
	}
	public long getFreeRuntimeMemory() {
		return freeRuntimeMemory;
	}
	public void setFreeRuntimeMemory(long freeRuntimeMemory) {
		this.freeRuntimeMemory = freeRuntimeMemory;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@Override
	public String toString() {
		return "InfraInformation [serviceName=" + serviceName + ", osName=" + osName + ", osVersion=" + osVersion
				+ ", osArchitecture=" + osArchitecture + ", cpuCount=" + cpuCount + ", hostname=" + hostname
				+ ", ipAddress=" + ipAddress + ", totalRuntimeMemory=" + totalRuntimeMemory + ", freeRuntimeMemory="
				+ freeRuntimeMemory + "]";
	}
	
	
	
}
